import './style';
import App from './components/app';
import { h, render } from 'preact';
import {reducers} from './store'
import store from './store'

import localForage from "localforage";

export default () => {
	render(h('div', {},'Chargement ...'), document.body);

	(localStorage.actions && localForage.setItem('actions', JSON.parse(localStorage.actions)) || localForage.getItem('actions'))
		.then( v => {
			return Promise.resolve(
				[].concat(v)
					.filter(Boolean)
					.filter(a => a.type)
					.reduce(reducers, {})
			).then (initValue => {
				render(h(App(store(initValue))), document.body, document.querySelector('div'));

				localStorage.removeItem('actions')
			});
		})

}