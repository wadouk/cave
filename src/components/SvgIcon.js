export default ({ href, fill = null }) => (<svg fill={fill} viewBox={'0 0 50 50'} width={50} height={50}>
	<use href={`/assets/${href}.svg#content`} />
</svg>);