export default [{
	color: '#641613',
	bubble: false,
	label: 'Rouge'
}, {
	color: '#ffe6a2',
	bubble: false,
	label: 'Blanc'
}, {
	color: '#ff95b6',
	bubble: false,
	label: 'Rosé'
}, {
	color: '#ffe6a2',
	bubble: true,
	label: 'Champagne'
}, {
	color: '#ffb463',
	bubble: true,
	label: 'Cidre'
}, {
	color: '#ff95b6',
	bubble: true,
	label: 'Champagne rosé'
}];