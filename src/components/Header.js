import { Link } from 'preact-router/match';
import { route } from 'preact-router';
import style from './Header.css';
import SvgIcon from './SvgIcon';
import { connect } from 'preact-redux';

const actions = {
	remove(e) {
		e.stopPropagation()
		e.preventDefault()
		return {
			type: 'REMOVE_PLACE'
		}
	},
	move(e) {
		e.stopPropagation()
		e.preventDefault()
		return {
			type: 'MOVE_BOTTLE'
		}
	}
}

const viewPlace =({placements, shouldDel}) => {
	return (e) => {
		e.stopPropagation()
		e.preventDefault()
		const a = shouldDel[0]
		const bottleId = placements.filter(p =>
			a.rackId === p.rackId
			&& a.editCell === p.editCell
			&& a.editLine === p.editLine)[0].id
		route(`/bottle/${bottleId}`)
	}
}

const shouldMove = (shouldAdd, shouldDel) => {
	return shouldAdd.length === shouldDel.length && shouldDel.length !== 0
}

export default connect(({ shouldAdd = [], bottles = [], shouldDel = [], placements }) => ({ shouldAdd, bottles, shouldDel, placements }), actions)(({shouldAdd, bottles, shouldDel, remove, placements, move}) => (
		<header class={style.header}>
			<nav>
				<ol>
					<li><Link href="/"><SvgIcon href={'home'}></SvgIcon></Link></li>
					<li>
						<ul>
							{shouldMove(shouldAdd, shouldDel) ? <li>
								<Link activeClassName={style.active} href="#" onClick={move}>
									<SvgIcon href={'move'} fill={'white'}/>
								</Link>
							</li> : undefined}
							{shouldMove(shouldAdd, shouldDel) || shouldAdd.length === 0 ? undefined : <li><Link activeClassName={style.active} href="/bottle">
								<SvgIcon href={'add'} fill={'white'}/>
							</Link></li>}
							{shouldMove(shouldAdd, shouldDel) || shouldDel.length === 0 ? undefined : <li>
								<a href='#' onClick={remove}><SvgIcon href={'minus'} fill={'white'}/></a>
							</li>}
							{shouldDel.length === 1 ? <li>
								<a href="#" onClick={viewPlace({placements, shouldDel})}><SvgIcon href={'glass'} fill={'white'}/></a>
							</li> : undefined}
							{bottles.length === 0 ? undefined : <li>
								<Link activeClassName={style.active} href="/bottles"><SvgIcon href={'bottle'}/></Link>
							</li>}
							{shouldAdd.concat(shouldDel).length === 0 ? <li>
								<Link activeClassName={style.active} href="/rack/new"><SvgIcon href={'cooler'}/></Link>
							</li> : undefined}
							{shouldAdd.concat(shouldDel).length === 0 ? <li>
								<Link activeClassName={style.active} href="/backup"><SvgIcon href={'backup'}/></Link>
							</li> : undefined}
						</ul>
					</li>
				</ol>
			</nav>
		</header>
	));