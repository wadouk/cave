import { connect } from 'preact-redux';
import { route } from 'preact-router';
import { Component, createRef } from 'preact';

const actions = {
	setBottlePicture (dataurl) {
		return {
			type: 'SET_BOTTLE_PICTURE',
			value: dataurl
		};
	},
	addBottle () {
		return {
			type: 'ADD_BOTTLE',
			id: Math.random().toString(36).substring(2)
		};
	}
};

const MAX_WIDTH = 1000

class PrerenderImage extends Component{
	constructor (props) {
		super(props)

		this.state = {...props}

	}

	bail = () => {
		this.setState({
			bottlePicture: undefined,
			enabled: false
		})
	}

	componentWillReceiveProps(nextProps, nextState) {
		this.setState({...nextProps})
	}

	loaded = () => {
		this.canvas.width = MAX_WIDTH;
		this.canvas.height = MAX_WIDTH * (this.img.height / this.img.width);

		var ctx = this.canvas.getContext("2d");
		ctx.drawImage(this.img, 0, 0, this.canvas.width, this.canvas.height);
		this.props.setBottlePicture(this.canvas.toDataURL('image/jpeg'))
		URL.revokeObjectURL(this.props.bottlePicture)
	}

	render(props, {	bottlePicture}) {
			return (<div style={{display: 'none'}}>
				<img src={bottlePicture} ref={img => this.img = img} onError={this.bail} onLoad={this.loaded}/>
				<canvas ref={this.canvas} ref={canvas => this.canvas = canvas}/>
			</div>)
	}
}

export const EditBottle = connect(({ bottlePicture, shouldAdd}) => ({bottlePicture, shouldAdd}), actions)(({ bottlePicture, setBottlePicture, addBottle, shouldAdd }) => (
		<div>
			<div>
				<input type="file" accept="image/*" onChange={(e) => {
					setBottlePicture(URL.createObjectURL(e.target.files[0]));
				}}/>
				<button onClick={_ => {
					route(`/rack/${shouldAdd[0].rackId}`);
					addBottle();
				}} disabled={!/^data:/.test(bottlePicture)}
				>Ajouter
				</button>
			</div>
			{/^blob/.test(bottlePicture) ? <PrerenderImage {...{bottlePicture, setBottlePicture}}/> : null}
			{/^data/.test(bottlePicture) ? <img src={bottlePicture} style={{marginLeft: -20, width: '100%'}}/> : null}
		</div>
	));
EditBottle.displayName = 'EditBottleComp';