import { connect } from 'preact-redux';
import { Link } from 'preact-router';
import colors from './colors'

import style from './ListBottles.css'

function getContrast50(hexcolor){
	return (parseInt(hexcolor, 16) > 0xffffff/2) ? 'black':'white';
}
function colorStyle(color) {
	let bgColor = colors.filter(p => p.label === color)[0];
	if (!color || !bgColor) return {}
	return {
		backgroundColor: bgColor.color,
		color: getContrast50(bgColor.color.slice(1))
	}
}

export default connect(({bottles, placements}) => ({bottles, placements}))(({bottles = [], placements}) => {

	return (
		<table class={style.listBottles}>
			{Object.entries(bottles)
                .map(([k, v]) => [k, {...v, nb: placements.filter(p => p.id === k).length}])
                .filter(([_, {nb}]) => !(nb === 0))
                .map(([k, {image, color = undefined, year = undefined, name = undefined, nb}]) => {
				return (<tr>
					<td height={130}>
						<Link href={`/bottle/${k}`}>
							<img src={image} width={130}/>
						</Link>
					</td>
					<td>{nb}</td>
					<td style={colorStyle(color)}>{color}</td>
					<td>{year}</td>
					<td>{name}</td>
				</tr>);
			})}
		</table>
	);
});