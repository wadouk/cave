import { connect } from 'preact-redux';
import {Link} from 'preact-router'
import colors from './colors'

function fill (length) {
	return new Array(length).fill(null);
}

const r = 15;
const size = ((r + 2) * 2);

const actions = {
	editBottle (rackId, editLine, editCell) {
		return {
			type: 'EDIT_BOTTLE',
			rackId, editCell, editLine
		};
	}
};

function getBottleColor(place, bottles) {
	if (place) {
		const color = colors.filter(c => c.label === bottles[place.id].color)[0]
		return color ? color.color : 'grey'
	}
	return 'white'
}

export default connect(({editBottle, shouldDel = [], shouldAdd = [], placements = [], bottles = {} }) => ({editBottle, shouldDel, shouldAdd, placements, bottles}), actions)
(({editBottle, shouldDel, shouldAdd, placements, rack, bottles}) => (
	<div>
		<h2><Link href={`/rack/${rack.id}`}>{rack.name}</Link> <Link href={`/rack/${rack.id}/edit`}>E</Link></h2>
		<svg viewPort={`0 0 ${rack.cell * size} ${rack.line * size}`}
				 width={rack.cell * size + size}
				 height={rack.line * size + size}
				 xmlns="http://www.w3.org/2000/svg"
		>{fill(rack.line).map((line, lineIndex) =>
			fill(rack.cell).map((cell, cellIndex) => {
				function filterIt ({ rackId, editCell, editLine }) {
					return rack.id === rackId && editCell === cellIndex && editLine === lineIndex;
				}

				return (
					<circle
						{...{
							r,
							cx: cellIndex * size + size,
							cy: lineIndex * size + size,
							stroke: 'black',
							'stroke-width': shouldAdd.concat(shouldDel).some(filterIt) ? 2 : 1,
							'stroke-dasharray': shouldDel.some(filterIt) ? '6' : '',
							fill: getBottleColor(placements.filter(filterIt)[0], bottles)
						}}
						onClick={_ => {editBottle(rack.id, lineIndex, cellIndex);}}
					/>);
			})
		)}</svg>
	</div>))