import { h } from 'preact';
import { connect } from 'preact-redux';
import { route } from 'preact-router';
import style from './NewRack.css';

const actions = {
	fixName (e) {
		return {
			type: 'FIX_NAME',
			value: e.target.value
		};
	},

	fixRack (rackId) {
		return ({
			type: 'FIX_RACK',
			id: rackId
		});
	}
};

export const EditRack = connect(
({racks}, {rackId}) => ({name: racks.filter(r => r.id === rackId)[0].name}), actions)
	(({ name, fixName, fixRack, rackId }) => (
	<div className={`${style.newRack}`}>
		<h3>Corriger le nom</h3>
		<ul>
			<li>
				<label htmlFor="">Nom</label>
				<input type="text" value={name} onChange={fixName} />
			</li>
			<li>
				<button onClick={e => {
					route('/');
					return fixRack(rackId);
				}}
				>Corriger
				</button>
			</li>
		</ul>
	</div>
));

EditRack.displayName = 'EditRackComp';