import { h, Component } from 'preact';
import { Router } from 'preact-router';
import { Provider } from 'preact-redux';

import Header from './Header';

import Home from '../routes/Home';
import NewRack from '../routes/NewRack';
import EditRack from '../routes/EditRack';
import EditBottle from '../routes/EditBottle';
import ListBottles from '../routes/ListBottles';
import ViewBottle from '../routes/ViewBottle';
import Backup from '../routes/Backup';

import style from './app.css'

export default (store) => class App extends Component {
	handleRoute = e => {
		this.setState({ currentUrl: e.url });
	};

	render() {
		return (
			<Provider store={store}>
				<div id="app">
					<Header />
					<div className={style.appContent}>
						<Router onChange={this.handleRoute}>
							<Home path="/" />
							<Home path="/rack/:id" />
							<EditRack path="/rack/:id/edit" />
							<NewRack path="/rack/new" />
							<EditBottle path="/bottle" />
							<ViewBottle path="/bottle/:id" />
							<ListBottles path="/bottles" />
							<Backup path="/backup" />
						</Router>
					</div>
				</div>
			</Provider>
		);
	}
}