import { h } from 'preact';
import { connect } from 'preact-redux';
import { route } from 'preact-router';
import style from './NewRack.css';

const actions = {
	setLine (e) {
		return {
			type: 'SET_LINE',
			value: parseInt(e.target.value, 10)
		};
	},

	setName (e) {
		return {
			type: 'SET_NAME',
			value: e.target.value
		};
	},

	setCell (e) {
		return ({
			type: 'SET_CELL',
			value: parseInt(e.target.value, 10)
		});
	},

	addRack () {
		return ({
			type: 'ADD_RACK',
			id: Math.random().toString(36).substring(2)
		});
	}

};

export const NewRack = connect(
	(state) => state ? {
		total: state.line && state.name && state.cell ? state.line * state.cell : false,
		...state
	} : {}, actions)(({ name, line, cell, total, setName, setLine, setCell, addRack }) => (
	<div className={`${style.newRack}`}>
		<h3>Nombre d'emplacements ?</h3>
		<ul>
			<li>
				<label htmlFor="">Nom</label>
				<input type="text" value={name} onChange={setName} />
			</li>
			<li>
				<label htmlFor="">Ligne</label>
				<input type="number" value={line} onChange={setLine} />
			</li>
			<li>
				<label htmlFor="">Emplacement</label>
				<input type="number" value={cell} onChange={setCell} />
			</li>
			<li>
				<label htmlFor="total">Bouteilles</label> {total}
			</li>
			<li>
				<button onClick={e => {
					route('/');
					return addRack(e);
				}} disabled={total === false}
				>Ajouter
				</button>
			</li>
		</ul>
	</div>
));

NewRack.displayName = 'NewRackComp';