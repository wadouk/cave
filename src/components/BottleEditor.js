/*
 année
 couleur (rouge / blanc / rosé / champagne / cidre / champagne rosé)
 région
 nom
*/
import { Component } from 'preact';
import colors from './colors';
import style from './BottleEditor.css'
import {route} from 'preact-router'

function getOptionColors () {
	return colors.map(({label}, i) => {
		return (<option key={i}>{label}</option>);
	});
}

class BottleEditor extends Component {
	save = (e) => {
		e.preventDefault();
		e.stopPropagation();
		this.props.save({
			name : document.forms[0].name.value,
			year : document.forms[0].year.value,
			region : document.forms[0].region.value,
			color: document.forms[0].color.value,
			id: this.props.id
		});
		history.back()
	};
	cancel = (e) => {
		e.preventDefault();
		e.stopPropagation();
		this.props.cancel({});
		history.back()
	};

	render = ({bottle : {name, year, region, color}}) => {
		return (
			<div class={style.bottleEditor}>
				<form action="#" name={'bottleEditor'}>
					<div>
						<label for="name">Nom</label>
						<input type="text" name={'name'} autocomplete='off' spellcheck={false} value={name}/>
					</div>
					<div>
						<label for="year">Année</label>
						<input type="number" name={'year'} value={year}/>
					</div>
					<div>
						<label for="region">Région</label>
						<input type="text" name={'region'} autocomplete='off' spellcheck={false} value={region}/>
					</div>
					<div>
						<label for="color">Couleur</label>
						<datalist list={'colors'} name={'colors'} id={'colors'}>
							{getOptionColors()}
						</datalist>
						<input name="color" list='colors' value={color}/>
					</div>
					<div>
						<button onClick={this.save}>Enregistrer</button>
						<button onClick={this.cancel}>Annuler</button>
					</div>
				</form>
			</div>
		);
	}
}

export default BottleEditor;