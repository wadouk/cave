import style from './backup.css'
import {Component} from 'preact'
import RemoteStorage from 'remotestoragejs'
import Widget from 'remotestorage-widget'
import localForage from "localforage";

const remoteStorage = new RemoteStorage({
	cache: false
})

remoteStorage.access.claim('cave', 'rw');

function saveIt () {
	return localForage.getItem('actions')
		.then(v => {
			return remoteStorage.scope('/cave/')
					.storeFile('plain/text', 'actions', JSON.stringify(v))
		})
}

function fetchIt () {
	return remoteStorage.scope('/cave/').getFile('actions')
		.then(r => localForage.setItem('actions', JSON.parse(r.data)))
		.then(_ => location.reload(true))
}

function resetIt() {
	localForage.removeItem('actions')
	location.reload(true)
}

class Backup extends Component {
	constructor (props) {
		super(props)
	}
	componentDidMount = () => {
		const widget = new Widget(remoteStorage)
		widget.attach(this.storageBind.id)
	}

	bindStorage = (e) => this.storageBind = e

	saveIt = () => {
		this.setState({save: 'wip'})
		saveIt()
			.then(_ => {
				this.setState({save: 'done'})
			})
			.catch(_ => {
				this.setState({save: 'err'})
			})
	}

	fetchIt = () => {
		this.setState({fetch: 'wip'})
		fetchIt()
			.then(_ => this.setState({fetch: 'done'}))
			.catch(_ => this.setState({fetch: 'err'}))
	}

	render = () => {
		return (<div className={style.store}>
			<h1>Sauvegarde</h1>
			<div id='storageWidget' ref={this.bindStorage}/>
			<form action="#" onSubmit={(e) => {
				e.preventDefault();
				e.stopPropagation();
			}}>
				<div>
					<button onClick={this.saveIt}>Enregistre</button>
				</div>
				<div>
					<button onClick={this.fetchIt}>Recupère</button>
				</div>
				<div>
					<button onClick={resetIt}>RAZ</button>
				</div>
				<div>{this.state.save}</div>
				<div>{this.state.fetch}</div>
				<div>{document.lastModified}</div>
			</form>
		</div>)
	}
}

export default () => (<Backup/>)