import { connect } from 'preact-redux';
import style from './Home.css';
import Rack from '../components/Rack';

let HomeRoute = connect(({racks = []}, {id = undefined}) => ({racks, id}))(({racks = [], id}) => (
		<div class={style.home}>
			<h1>Casiers</h1>
			{racks.filter(r => !id || (id && r.id === id)).map(rack => <Rack rack={rack}/>)}
		</div>
	));
HomeRoute.displayName = 'HomeRoute';
export default HomeRoute;