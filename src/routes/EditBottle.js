import { EditBottle } from '../components/EditBottle';

export default ({ rackId, editLine, editCell }) => (
	<div>
		<h1>Editer une bouteille</h1>
		<EditBottle rackId={rackId} editLine={parseInt(editLine, 10)} editCell={parseInt(editCell, 10)} />
	</div>
);