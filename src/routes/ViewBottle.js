import { connect } from 'preact-redux';
import { Link } from 'preact-router/match';
import BottleEditor from '../components/BottleEditor'
import style from './ViewBottle.css'

const actions = {
	save: (v) => ({
		type: 'BOTTLE_DETAILS',
		...v
	}),
	cancel: () => ({
		type: 'BOTTLE_DETAILS_CANCEL',
	})
}
export default connect(({bottles, placements, racks}, {id}) => ({
	bottles,
	id,
	placements,
	racks
}), actions)(({bottles, id, placements, racks, save, cancel}) => {
	let bottlePlacements = placements.filter(p => p.id === id);

	const ps = bottlePlacements.reduce((a, b) => {
		a[b.rackId] = (a[b.rackId] || []).concat({...b}).filter(t => Object.keys(t).length !== 0);
		return a;
	}, {});

	return (
		<div class={style.viewBottle}>
			<div>
				<div>{bottlePlacements.length} bouteille{bottlePlacements.length > 1 ? 's' : undefined}</div>
				<div>{Object.entries(ps).map(([k, v]) => {
					const rackName = racks.filter(r => r.id === k)[0].name;
					return (
						<div>
							<Link href={`/rack/${k}`}>{rackName}</Link>
							<span> {v.map(p => `l:${p.editLine+1}-c:${p.editCell+1}`).join(', ')}</span></div>)
				})}
				</div>
				<div className={style['viewBottle-outer-img']}>
					<div className={style['viewBottle-inner-img']}>
						<img src={bottles[id].image} style={{maxWidth: "100vw", maxHeight: "calc(100vh - 10em)"}}/>
					</div>
				</div>
			</div>
			<div class={style['viewBottle-editor']}>
				<BottleEditor id={id} save={save} cancel={cancel} bottle={bottles[id]}/>
			</div>
		</div>);
});