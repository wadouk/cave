import { h } from 'preact';
import { EditRack } from '../components/EditRack';

export default ({id}) => <EditRack rackId={id}/>