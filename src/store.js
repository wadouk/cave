import { applyMiddleware, compose, createStore } from 'redux';
import localForage from "localforage";

const matchBy = (action) =>
	({rackId, editCell, editLine}) =>
		rackId === action.rackId && editCell === action.editCell && editLine === action.editLine;

const not = (fn) => (...args) => !fn(...args);

export function reducers (state, {type, ...action}) {
	try {
		return ({
			SET_LINE: ({line, ...state}, action) => ({
				line: action.value,
				...state
			}),
			SET_NAME: ({name, ...state}, action) => ({
				name: action.value,
				...state
			}),
			FIX_NAME: ({name, ...state}, action) => ({
				name: action.value,
				...state
			}),
			SET_CELL: ({cell, ...state}, action) => ({
				cell: action.value,
				...state
			}),
			ADD_RACK: ({line, name, cell, racks = [], ...restState}, {id}) => ({
				line: undefined, name: undefined, cell: undefined,
				racks: racks.concat({
					line, name: name.trim(), cell, id
				}),
				...restState
			}),
			FIX_RACK: ({name, racks = [], ...restState}, {id}) => {
				const r0 = racks.map( (r, i) => ({...r, i}));
				const newR = r0
					.filter(r => r.id === id)
					.map(r => ({...r, name: name.trim()}));
				const otherR = r0.filter(r => r.id !== id);
				const newRs = [].concat(newR).concat(otherR);
				newRs.sort((a, b) => (a.i > b.i ? 1 : -1));
				return {
					name: undefined,
					racks: newRs.map(({i, ...r}) => r),
					...restState
				}
			},
			EDIT_BOTTLE: ({shouldAdd = [], placements = [], shouldDel = [], ...state}, {...action}) => {
				return {
					shouldAdd: (() => {
						if (!shouldAdd.some(matchBy(action))) {
							if (!placements.some(matchBy(action))) {
								return shouldAdd.concat({...action});
							}
							return shouldAdd;
						}

						return shouldAdd.filter(not(matchBy(action)));
					})(),
					shouldDel: (() => {
						if (!placements.some(matchBy(action))) {
							return shouldDel;
						}
						if (shouldDel.some(matchBy(action))) {
							return shouldDel.filter(not(matchBy(action)));
						}
						return shouldDel.concat({...action});

					})(),
					placements,
					...state
				};
			},
			MOVE_BOTTLE: ({shouldAdd, shouldDel, placements, ...store}) => {
				return {
					shouldAdd: [],
					shouldDel: [],
					placements: placements
						.filter(a => !shouldDel.some(b => matchBy(b)(a)))
						.concat(shouldAdd.map((a, i) =>({...a, id: placements.find(matchBy(shouldDel[i])).id}))),
					...store
				}
			},
			SET_BOTTLE_PICTURE: ({bottlePicture, ...state}, {value}) => ({
				bottlePicture: value,
				...state
			}),
			REMOVE_PLACE: ({placements, shouldDel, ...state}) => ({
				placements: placements.filter(p => !shouldDel.some(matchBy(p))),
				shouldDel: [],
				...state
			}),
			BOTTLE_DETAILS: ({bottles, ...state}, {name, year, region, color, id}) => {
				const newV = {name, region, year, color};
				return ({
					bottles: Object.entries(bottles).map(([k, {image, ...v}]) => {
						return k === id ? {...newV, image, k} : {image, k, ...v}
					}).reduce((a, {k, ...b}) => ({...{[k]: b}, ...a}), {}),
					...state,
					shouldDel: [],
				})
			},
			BOTTLE_DETAILS_CANCEL: (state) => {
				return ({
					...state,
					shouldDel: []
				})
			},
			ADD_BOTTLE: ({bottlePicture, shouldAdd, placements = [], bottles = {}, ...restState}, {id}) => ({
				bottlePicture: undefined,
				shouldAdd: undefined,
				placements: placements.concat(shouldAdd.filter(p => !placements.some(matchBy(p))).map(p => ({...p, id}))),
				bottles: { ...bottles, ...{ [id]: {image: bottlePicture }} }, ...restState
			})
		})[type](state, action);
	} catch (e) {
		return state || {};
	}
}

let enhancer =
	typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

export default (initValue) => createStore(
	reducers,
	initValue,
	enhancer(applyMiddleware(() => next => action => {
		localForage.getItem('actions').then((v) => {
			localForage.setItem('actions', (v || '[]')
				.concat(action)
				.filter(Boolean)
				.filter(a => a.type))
				.then(_ => {
					next(action)
				});
		})
	}))
);