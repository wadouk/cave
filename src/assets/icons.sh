#!/usr/bin/env bash
convert -resize 512x512 src/assets/bottle.svg src/assets/icons/android-chrome-512x512.png
convert -resize 192x192 src/assets/bottle.svg src/assets/icons/android-chrome-192x192.png
convert -resize 180x180 src/assets/bottle.svg src/assets/icons/apple-touch-icon.png
convert -resize 16x16 src/assets/bottle.svg src/assets/icons/favicon-16x16.png
convert -resize 32x32 src/assets/bottle.svg src/assets/icons/favicon-32x32.png
convert -resize 150x150 src/assets/bottle.svg src/assets/icons/mstile-150x150.png
convert -resize 16x16 src/assets/bottle.svg src/assets/favicon.ico
